# My portfolio website

## Getting Started

To run development server

```bash
yarn install

yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

### Using

- Next.js
- TypeScript
- Tailwind CSS

### ToDo

- [x] Setup navbar and navigation
- [x] Try ipad-cursor
- [x] Make a basic homepage
- [x] Add some animation to the homepage
- [x] Improve the homepage
- [x] Add background animation instead of text animation (th is that flickering lel)
- [ ] Work on pages
  - [x] About
  - [x] Projects
  - [x] Blog
  - [x] Notes
  - [x] Contact
- [x] Refactor and clean the code
