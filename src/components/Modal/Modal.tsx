import { useEffect, useRef } from 'react';
import { XSquare } from '@phosphor-icons/react';
import "./Modal.css";

interface ModalProps {
  onClose: () => void;
  children: React.ReactNode;
}

const Modal: React.FC<ModalProps> = ({ onClose, children }) => {
  const modalRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const handleEscapeKey = (event: KeyboardEvent) => {
      if (event.key === 'Escape') {
        onClose();
      }
    };

    const handleClickOutside = (event: MouseEvent) => {
      if (modalRef.current && !modalRef.current.contains(event.target as Node)) {
        onClose();
      }
    };

    document.addEventListener('keydown', handleEscapeKey);
    document.addEventListener('click', handleClickOutside);

    return () => {
      document.removeEventListener('keydown', handleEscapeKey);
      document.removeEventListener('click', handleClickOutside);
    };
  }, [onClose]);

  return (
    <div className="cust-modal fixed inset-0 flex items-start justify-center z-50">
      <div className="modal-bg fixed inset-0"></div>
      <div className="main-modal relative top-10 w-full max-h-full overflow-y-auto p-8 shadow-lg">
        <button
          className="modal-close-btn absolute top-0 right-0 m-4"
          onClick={onClose}
        >
          <XSquare size={32} weight="duotone" />
        </button>
        <div ref={modalRef} className="break-all">{children}</div>
      </div>
    </div>
  );
};

export default Modal;
