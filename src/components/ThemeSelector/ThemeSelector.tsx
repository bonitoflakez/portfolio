import { useState, useEffect } from "react";
import "./ThemeSelector.css";

export default function ThemeSelector() {
  const [selectedTheme, setSelectedTheme] = useState<string>("gruv-dark");

  const setTheme = (theme: string) => {
    setSelectedTheme(theme);
    localStorage.setItem("color-scheme", theme);
    document.documentElement.setAttribute("data-theme", theme);
  };

  useEffect(() => {
    const userPreference = localStorage.getItem("color-scheme");
    if (userPreference) {
      setSelectedTheme(userPreference);
      document.documentElement.setAttribute("data-theme", userPreference);
    } else {
      setTheme("gruv-dark");
    }
  }, []);

  return (
    <div className="theme-selector-container">
      <div className="theme-selector fixed bottom-0 right-0 m-2">
        <div className={`theme-checker ${selectedTheme}`} />
        <select
          value={selectedTheme}
          onChange={(e) => setTheme(e.target.value)}
          className="theme-select p-1 border rounded-md"
        >
          <option value="gruv-dark">Gruvbox Dark</option>
          <option value="dark-void">Dark Void</option>
          <option value="gruv-light">Gruvbox Light</option>
          <option value="light-void">Light Void</option>
        </select>
      </div>
    </div>
  );
}
