import { useState, useEffect } from "react";
import Link from "next/link";
import { PawPrint } from "@phosphor-icons/react";
import "./Navbar.css";

const Navbar: React.FC = () => {
  return (
    <nav className="navbar flex items-center justify-between p-4 backdrop-blur-sm">
      <div className="bonito-logo">
        <Link data-cursor="text" href="/" className="text-xl">
          <span className="text-logo">
            <PawPrint size={32} weight="duotone" />
          </span>
          <span className="text-logo-smol">
            <PawPrint size={32} weight="regular" />
          </span>
        </Link>
      </div>
      <div className="nav-links space-x-4">
        <Link className="nav-link p-1.5" href="/">
          <span>Home</span>
        </Link>
        <Link className="nav-link p-1.5" href="/projects">
          <span>Projects</span>
        </Link>
        <Link className="nav-link p-1.5" href="/notes">
          <span>Notes</span>
        </Link>
      </div>
    </nav>
  );
};

export default Navbar;
