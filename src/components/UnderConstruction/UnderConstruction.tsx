import "./UnderConstruction.css";

const UnderConstruction = () => {
  return (
    <div className="under-construction">
      <h1 className="under-construction-heading">WIP</h1>
      <p className="under-construction-text">This page is currently under construction</p>
    </div>
  );
};

export default UnderConstruction;
