"use client";

import { motion } from "framer-motion";
import { textVariants, subStuffVariants, blobVariants } from "../utils/motions";
import { useState, useEffect } from "react";
import { TwitterLogo, DiscordLogo, LinkedinLogo, At, MediumLogo } from "@phosphor-icons/react";
import { linkVariants } from "../utils/motions";
import Image from "next/image";
import Link from "next/link";
import "./Home.css";
import ThemeSelector from "@/components/ThemeSelector/ThemeSelector";

export default function About() {

  const calculateAge = (dob: string) => {
    const birthDate = new Date(dob);
    const currentDate = new Date();
    let age = currentDate.getFullYear() - birthDate.getFullYear();
    const monthDifference = currentDate.getMonth() - birthDate.getMonth();
    if (monthDifference < 0 || (monthDifference === 0 && currentDate.getDate() < birthDate.getDate())) {
      age -= 1;
    }
    return age;
  };

  const [age, setAge] = useState(calculateAge("2004-08-17"));

  useEffect(() => {
    setAge(calculateAge("2004-08-17"));
  }, []);

  return (
    <>
      <motion.div
        className="about-section"
        initial="hidden"
        animate="visible"
        variants={textVariants}
      >
        <motion.h1
          variants={textVariants}
          className="text-center mb-4 pt-2 text-2xl font-extrabold tracking-tight md:text-4xl lg:text-5xl"
        >
          About me{" "}
        </motion.h1>
        <div className="image-container mt-4">
          <Image
            src={"/pfp.jpg"}
            alt="pfp"
            width={300}
            height={300}
            className="pfp rounded-md mx-auto shadow"
          />
        </div>
        <div className="about-bullets text-center mt-6">
          <span className="px-4 py-2 mr-0.5 ml-0.5">
            {age}
          </span>
          <span className="px-4 py-2 mr-0.5 ml-0.5">
            INFP
          </span>
          <span className="px-4 py-2 mr-0.5 ml-0.5">
            Apache/Apachim
          </span>
          <span className="px-4 py-2 mr-0.5 ml-0.5">
            =￣ω￣=
          </span>
        </div>
        <motion.p
          variants={textVariants}
          className="inner-txt-container mt-4 text-center mb-2 2xl:px-48"
        >
          My name's <span className="text-1xl highlight">Nikhil,</span> you might also
          know me by my pseudonym{" "}
          <span className="text-1xl highlight">BonitoFlakez</span>.
          <br />
          I'm a {" "}
          <span className="text-1xl highlight">student</span> and a{" "}
          <span className="text-1xl highlight">developer</span>.
          Apart from coding I love reading <span className="text-1xl highlight">books</span>, playing <span className="text-1xl highlight">games</span> and <span className="text-1xl highlight">doodling</span>.
          <br />
          <br />
          <motion.div
            initial="hidden"
            animate="visible"
            variants={linkVariants}
            className="social-links flex pb-2 justify-center"
          >
            <Link
              className="p-2 social-link"
              href={"https://discord.com/users/902587623573426226"}
            >
              <DiscordLogo size={38} weight="light" />
            </Link>
            <Link
              className="p-2 social-link"
              href={"https://bonitoflakez.medium.com/"}
            >
              <MediumLogo size={38} weight="light" />
            </Link>
            <Link
              className="p-2 social-link"
              href={"https://www.linkedin.com/in/nikhil-dhiman-b81326211/"}
            >
              <LinkedinLogo size={38} weight="light" />
            </Link>
            <Link className="p-2 social-link" href={"mailto:nikhildhiman9050@gmail.com"}>
              <At size={38} weight="light" />
            </Link>
          </motion.div>
          <motion.h2 variants={textVariants} className="text-2xl font-bold">
            Stuff I use
          </motion.h2>
          <motion.div variants={subStuffVariants} className="container mx-auto px-4 py-8">
            <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-2">
              <div className="stuff px-4 py-2 mr-0.5 ml-0.5">
                <h2 className="text-1xl highlight">Frontend</h2>
                <p>React.js - Next.js - TailwindCSS</p>
              </div>
              <div className="stuff px-4 py-2 mr-0.5 ml-0.5">
                <h2 className="text-1xl highlight">Backend</h2>
                <p>Go - Node.js/TypeScript</p>
              </div>
              <div className="stuff px-4 py-2 mr-0.5 ml-0.5">
                <h2 className="text-1xl highlight">CLI Utils</h2>
                <p>Go - Shell - Python - Node.js - Rust</p>
              </div>
              <div className="stuff px-4 py-2 mr-0.5 ml-0.5">
                <h2 className="text-1xl highlight">Code Editor</h2>
                <p>VSCode - NeoVim</p>
              </div>
              <div className="stuff px-4 py-2 mr-0.5 ml-0.5">
                <h2 className="text-1xl highlight">Notes</h2>
                <p>Markdown - LaTeX - Obsidian</p>
              </div>
              <div className="stuff px-4 py-2 mr-0.5 ml-0.5">
                <h2 className="text-1xl highlight">OS</h2>
                <p>Win 11 - WSL2 running Ubuntu - Arch Linux</p>
              </div>
            </div>
          </motion.div>
        </motion.p>
      </motion.div >
      <ThemeSelector />
    </>
  );
}
