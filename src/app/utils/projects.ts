interface Projects {
  id: number;
  title: string;
  description: string;
  gitlabLink: string;
  exampleLink: string;
}

const projects: Projects[] = [
  {
    id: 1,
    title: "GOAT",
    description: "An API tracer CLI utility written in Go",
    gitlabLink: "https://gitlab.com/bonitoflakez/goat",
    exampleLink: "",
  },
  {
    id: 2,
    title: "goJSON2CLASS",
    description: "A utility that converts JSON schema to classes",
    gitlabLink: "https://gitlab.com/bonitoflakez/goJSON2CLASS",
    exampleLink: "",
  },
  {
    id: 3,
    title: "go-2FA-app",
    description: "Two-factor authentication implementation in Go",
    gitlabLink: "https://gitlab.com/bonitoflakez/go-2FA-app",
    exampleLink: "",
  },
  {
    id: 4,
    title: "Bashy",
    description: "A system fetch script",
    gitlabLink: "https://gitlab.com/bonitoflakez/bashy",
    exampleLink: "",
  },
  {
    id: 5,
    title: "Portfolio Website",
    description: "This website",
    gitlabLink: "https://gitlab.com/bonitoflakez/portfolio/",
    exampleLink: "",
  },
];

export { projects };
