interface Notes {
  id: number;
  title: string;
  description: string;
  data: string;
}

export const noteData: Notes[] = [
  {
    id: 1,
    title: "WSL Setup",
    description: "Quick WSL Setup",
    data: "## My quick devy WSL setup\n> Make sure you have an WSL and Ubuntu installed on WSL\n\n[How to install WSL](https://learn.microsoft.com/en-us/windows/wsl/install)\n### Install basic utils\n```sh\nsudo apt update -y && sudo apt install git wget curl zip unzip gzip p7zip tar neofetch vim neovim golang python3 python3-pip rustc cargo nodejs npm gcc gdb g++ make cmake build-essential zsh -y\n```\n### Install OhMyZsh\n[Oh My ZSH Website](https://ohmyz.sh/)\n```sh\nsh -c '$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)'\n```\nSome plugins that I just love\n- [zsh-autosuggestions](https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md#oh-my-zsh)\n- [zsh-syntax-highlighting](https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/INSTALL.md) \n\nClone both zsh-autosuggestions and zsh-syntax-highlighting plugins\n```sh\ngit clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions && git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting\n```\nAdd them to `.zshrc`\n```sh\nsed -i 's/plugins=(git)/plugins=(git zsh-autosuggestions zsh-syntax-highlighting)/' ~/.zshrc && source .zshrc\n```\nOR just copy paste this in your `.zshrc`\n```zsh\nexport ZSH=\"$HOME/.oh-my-zsh\"\n# More themes on: https://github.com/ohmyzsh/ohmyzsh/wiki/Themes\nZSH_THEME=\"robbyrussell\"\n# true/false\nCASE_SENSITIVE=\"false\"\n# zstyle ':omz:update' mode disabled\n# disable automatic updates\n# zstyle ':omz:update' mode auto\n# update automatically without asking\n# zstyle ':omz:update' mode reminder\n# just remind me to update when it's time\nplugins = (\n\tgit\n\tzsh-autosuggestions\n\tzsh-syntax-highlighting\n)\nsource $ZSH/oh-my-zsh.sh\n# User configuration\nexport LANG=en_US.UTF-8\n# Preferred editor for local or remote session\nif [[ -n $SSH_CONNECTION ]]; then\n\texport EDITOR='vim'\nelse\n\texport EDITOR='nvim'\nfi\n# aliases\nalias zshconf=\"nvim ~/.zshrc\"\nalias ohmyzsh=\"nvim ~/.oh-my-zsh\"\n```",
  },
];
