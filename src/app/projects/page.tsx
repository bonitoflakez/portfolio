"use client";

import React from "react";
import { motion } from "framer-motion";
import { GitlabLogo, Globe } from "@phosphor-icons/react";
import { projects } from "../utils/projects";
import { cardsVariants, textVariants } from "../utils/motions";
import "./Projects.css";
import ThemeSelector from "@/components/ThemeSelector/ThemeSelector";

const Projects = () => {
  return (
    <>
      <div className="projects-section">
        <motion.h1
          initial="hidden"
          animate="visible"
          variants={textVariants}
          className="projects-heading text-center mb-4 pt-2 text-2xl font-extrabold tracking-tight md:text-4xl lg:text-5xl"
        >
          Projects
        </motion.h1>
        <motion.div
          initial="hidden"
          animate="visible"
          variants={cardsVariants}
          className="projects-card-grid-container grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8 mt-8 pb-8 pl-8 pr-8"
        >
          {projects.map((project) => (
            <div
              key={project.id}
              className="project-cards rounded-lg p-8 shadow"
            >
              <h2 className="font-bold mb-2">{project.title}</h2>
              <p>{project.description}</p>
              <div className="flex mt-4">
                <a
                  href={project.gitlabLink}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="btn-gitlab"
                >
                  <GitlabLogo size={32} weight="duotone" />
                </a>
                {project.exampleLink && (
                  <a
                    href={project.exampleLink}
                    target="_blank"
                    rel="noopener noreferrer"
                    className="btn-example ml-4"
                  >
                    <Globe size={32} weight="duotone" />
                  </a>
                )}
              </div>
            </div>
          ))}
        </motion.div>
      </div>
      <ThemeSelector />
    </>
  );
};

export default Projects;
