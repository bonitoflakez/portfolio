"use client";

import { useState } from 'react';
import { motion } from 'framer-motion';
import { cardsVariants, textVariants } from '../utils/motions';
import ReactMarkdown from 'react-markdown';
import remarkGfm from 'remark-gfm'
import Modal from "../../components/Modal/Modal";
import ThemeSelector from '@/components/ThemeSelector/ThemeSelector';
import { noteData } from '../utils/notesData';
import "./Notes.css";

interface NotesInter {
  id: number;
  title: string;
  description: string;
  data: string;
}

export default function Notes() {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedNote, setSelectedNote] = useState<NotesInter | null>(null);

  const openModal = (note: NotesInter) => {
    setSelectedNote(note);
    setIsOpen(true);
  };

  const closeModal = () => {
    setSelectedNote(null);
    setIsOpen(false);
  };

  return (
    <>
      <div className="notes-container">
        <motion.h1
          initial="hidden"
          animate="visible"
          variants={textVariants}
          className="notes-page-heading text-center mb-4 pt-2 text-2xl font-extrabold tracking-tight md:text-4xl lg:text-5xl"
        >
          Notes
        </motion.h1>
        <motion.div
          initial="hidden"
          animate="visible"
          variants={cardsVariants}
          className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-6 gap-6 p-6"
        >
          {noteData.map((note: NotesInter) => (
            <div key={note.id} className="notes-card-container p-4 shadow">
              <h2 className="notes-card-title text-xl font-bold mb-2">{note.title}</h2>
              <button className="notes-card-button" onClick={() => openModal(note)}>
                Read more
              </button>
            </div>
          ))}
        </motion.div>
        {isOpen && selectedNote && (
          <Modal onClose={closeModal}>
            <h2 className="notes-modal-title text-2xl text-center font-bold">{selectedNote.title}</h2>
            <div className='notes-modal-data-container'>
              <ReactMarkdown remarkPlugins={[remarkGfm]} className="notes-modal-data">
                {selectedNote.data}
              </ReactMarkdown>
            </div>
          </Modal>
        )}
      </div>
      <ThemeSelector />
    </>
  );
}