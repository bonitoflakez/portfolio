"use client";

import React, { ReactNode, useEffect, useState } from "react";
import dynamic from "next/dynamic";
import Navbar from "../components/Navbar/Navbar";
import "./globals.css";

interface RootLayoutProps {
  children: ReactNode;
}

const AnimatedCursor = dynamic(() => import("react-animated-cursor"), {
  ssr: false,
});

const RootLayout: React.FC<RootLayoutProps> = ({ children }) => {
  const [isMobileView, setIsMobileView] = useState<boolean>(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMobileView(window.innerWidth < 768);
    };

    handleResize();

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <>
      {isMobileView ? (
        <html lang="en">
          <head>
            <title>BonitoFlakez</title>
          </head>
          <body>
            <Navbar />
            {children}
          </body>
        </html>
      ) : (
        <html lang="en">
          <head>
            <title>BonitoFlakez</title>
            <link rel="icon" type="image/x-icon" href="/favicon.ico" />
          </head>
          <body>
            <AnimatedCursor
              innerSize={8}
              outerSize={16}
              color="var(--crsr-clr)"
              outerAlpha={0.2}
              innerScale={0.7}
              outerScale={3}
              clickables={[
                "a",
                'input[type="text"]',
                'input[type="email"]',
                'input[type="number"]',
                'input[type="submit"]',
                'input[type="image"]',
                'label[for]',
                "select",
                "option",
                "textarea",
                "button",
                ".link",
              ]}
            />
            <Navbar />
            {children}
          </body>
        </html>
      )}
    </>
  );
};

export default RootLayout;
